#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import sys
from string import Template, uppercase, digits
from random import SystemRandom as sysrand

import settings

if sys.version_info.major < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')


class ConfTemplate(Template):
    delimiter = '%'
    idpattern = r'[a-z][_a-z0-9]*(\.[a-z][_a-z0-9]*)*'


def remake_string(str, vars_dict):
    temp = ConfTemplate(str)
    return temp.safe_substitute(vars_dict)


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def rewrite_joomla_conf(path_file, vars_dict):
    _file = open(path_file, 'r+')
    file_content = _file.read()
    new_file_content = remake_string(file_content, vars_dict)
    _file.seek(0)
    _file.write(new_file_content)
    _file.truncate()
    _file.close()


def _rand_char():
    return sysrand().choice(uppercase + digits)


def random_string(strlen):
    random_arr = [_rand_char() for _ in xrange(strlen)]
    return ''.join(random_arr)


def get_site_folder(site_name):
    return site_name


def get_site_dbname(site_name):
    repd = {'.': '_', ' ': ''}
    return replace_all(site_name, repd) + '_db'


def get_site_dbuser(site_name):
    repd = {'.': '_', ' ': ''}
    return replace_all(site_name, repd) + '_user'
