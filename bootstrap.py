#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from docker.client import Client
from os import path, makedirs, chmod

import settings


def start_mysql(client):
    if not len(client.containers(filters={'name': 'mysql'})):
        ENV_OPTIONS = {
            'MYSQL_ROOT_PASSWORD': settings.MYSQL_PASS
        }
        mysql_id = client.create_container(image=settings.MYSQL_IMG,
                                           detach=True,
                                           environment=ENV_OPTIONS,
                                           name='mysql')
        client.start(mysql_id,
                     binds={
                         settings.DIRS_STRUCT['sockets_mysql']:
                         {
                             'bind': '/run',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['confs_mysql']:
                         {
                             'bind': '/etc/mysql/conf.d',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['mysql_data']:
                         {
                             'bind': '/var/lib/mysql',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['logs_mysql']:
                         {
                             'bind': '/var/log/mysql',
                             'ro': False
                         }
                     })
        print('mysql running')
        return mysql_id
    else:
        print('mysql is already running')


def start_nginx(client):
    if not len(client.containers(filters={'name': 'nginx'})):
        nginx_id = client.create_container(image=settings.NGINX_IMG,
                                           detach=True,
                                           name='nginx',
                                           ports=[80],
                                           volumes=['/var/www',
                                                    '/etc/nginx/conf.d'])
        client.start(nginx_id,
                     port_bindings={80: ('127.0.0.1', 8000)},
                     binds={
                         settings.DIRS_STRUCT['sites_data']:
                         {
                             'bind': '/var/www',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['sockets_phpfpm']:
                         {
                             'bind': '/var/run/phpfpm',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['confs_nginx']:
                         {
                             'bind': '/etc/nginx/included',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['confs_nginx']+'/conf.d':
                         {
                             'bind': '/etc/nginx/conf.d',
                             'ro': False
                         },
                         settings.DIRS_STRUCT['logs_nginx']:
                         {
                             'bind': '/var/log/nginx',
                             'ro': False
                         }
                     })
        print('nginx running')
        return nginx_id
    print('nginx is already running')


def make_dirs():
    if not path.exists(settings.MAIN_FOLDER):
        makedirs(settings.MAIN_FOLDER)
        print('make dir ' + settings.MAIN_FOLDER)
    for cur_dir in sorted(settings.DIRS_STRUCT.values()):
        if not path.exists(cur_dir):
            makedirs(cur_dir)
            print('make dir ' + cur_dir)


def main():
    make_dirs()
    # TODO: rewrite this shit!
    # chmod(settings.DIRS_STRUCT['mysql_data'], 0o777)
    docker = Client(base_url=settings.DOCKER_SOCKET)
    #start_mysql(docker)
    start_nginx(docker)

if __name__ == '__main__':
    main()
