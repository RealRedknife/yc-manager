#!/usr/bin/python2.7

from os import path

# DAEMON
DAEMON_LOGS_DIR = '/var/log/ycmanager'
DAEMON_STDIN_PATH = '/dev/null'
DAEMON_STDOUT_PATH = '/dev/tty'
DAEMON_STDERR_PATH = '/dev/tty'
DAEMON_PIDFILE_PATH = '/var/run/ycmanager.pid'
LOGFORMAT = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s'

# DOCKER
DOCKER_SOCKET = 'unix://var/run/docker.sock'
NGINX_IMG = 'nginx'
PHP_IMG = 'php'
MYSQL_IMG = 'mysql'

RESTART_POLICY = {
    "MaximumRetryCount": 10,
    "Name": "always"
}

# REDIS
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0

# SERVER
SERVER_DOMAIN = 'yacr.ru'

# DEBUG
MYSQL_DEBUG = True

# FOLDER STRUCT
MAIN_FOLDER = '/ycmanager'

DIRS_STRUCT = {
    'confs': MAIN_FOLDER + '/confs',
    'confs_nginx': MAIN_FOLDER + '/confs/nginx',
    'confs_mysql': MAIN_FOLDER + '/confs/mysql',
    'confs_phpfpm': MAIN_FOLDER + '/confs/phpfpm',

    'logs': MAIN_FOLDER + '/logs',
    'logs_nginx': MAIN_FOLDER + '/logs/nginx',
    'logs_mysql': MAIN_FOLDER + '/logs/mysql',
    'logs_phpfpm': MAIN_FOLDER + '/logs/phpfpm',

    'sockets': MAIN_FOLDER + '/sockets',
    'sockets_phpfpm': MAIN_FOLDER + '/sockets/phpfpm',
    'sockets_mysql': MAIN_FOLDER + '/sockets/mysql',

    'sites_data': MAIN_FOLDER + '/sites_data',
    'mysql_data': MAIN_FOLDER + '/mysql_data',
    'sql_dumps': MAIN_FOLDER + '/sql_dumps'
}

SITE_FOLDERS = {
    'host_site_data': path.join(DIRS_STRUCT['sites_data'],
                                '%{site_folder}'),

    'cont_site_data': '/var/www/%{site_folder}',

    'host_socket_dir': path.join(DIRS_STRUCT['sockets_phpfpm'],
                                 '%{site_name}'),

    'host_mysql_socket_dir': DIRS_STRUCT['sockets_mysql'],

    'cont_mysql_socket_dir': '/var/run/mysqld',

    'cont_socket_dir': '/var/run/phpfpm',

    'host_log_dir': path.join(DIRS_STRUCT['logs_phpfpm'], '%{site_name}'),

    'cont_log_dir': '/var/log/phpfpm'
}

# MYSQL
MYSQL_HOST = 'localhost'
MYSQL_USER = 'root'
MYSQL_PASS = 'pass'
MYSQL_SOCKET = DIRS_STRUCT['sockets_mysql'] + '/mysqld.sock'

# SITE BINDS
PHP_BINDS = {
    SITE_FOLDERS['host_socket_dir']:
    {
        'bind': SITE_FOLDERS['cont_socket_dir'],
        'ro': False
    },
    SITE_FOLDERS['host_site_data']:
    {
        'bind': SITE_FOLDERS['cont_site_data'],
        'ro': False
    },
    SITE_FOLDERS['host_mysql_socket_dir']:
    {
        'bind': SITE_FOLDERS['cont_mysql_socket_dir'],
        'ro': False
    },
    SITE_FOLDERS['host_log_dir']:
    {
        'bind': SITE_FOLDERS['cont_log_dir'],
        'ro': False
    },
    '/ycmanager/sbin/yacr.php':
    {
        'bind': '/var/www/%{site_folder}/bin/yacr.php',
        'ro': False
    }
}

# JOOMLA FILES
JOOMLA_FOLDER = MAIN_FOLDER + '/joomla/pack1'
SQLDUMP = DIRS_STRUCT['sql_dumps'] + '/dump.sql'

# YACR FRONTEND
FRONTEND_IMG = 'yacr'

FRONTEND_BIND = {
    DIRS_STRUCT['sockets_phpfpm']:
    {
        'bind': SITE_FOLDERS['cont_socket_dir'],
        'ro': False
    },
    path.join(DIRS_STRUCT['sites_data'],
              'yacr.ru'):
    {
        'bind': '/var/www/yacr.ru',
        'ro': False
    },
    SITE_FOLDERS['host_mysql_socket_dir']:
    {
        'bind': SITE_FOLDERS['cont_mysql_socket_dir'],
        'ro': False
    },
    SITE_FOLDERS['host_log_dir']:
    {
        'bind': SITE_FOLDERS['cont_log_dir'],
        'ro': False
    }
}

# RABBITMQ
QUEUES = ['create_site',
          'start_site',
          'stop_site',
          'restart_site',
          'remove_site',
          'frontend']
