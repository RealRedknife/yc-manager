#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import sys
import logging
import pika
import json
from daemon import runner

import redis

import settings
from BaseManager import BaseManager
from FrontendManager import FrontendManager

if sys.version_info.major < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')

logging.basicConfig(format=settings.LOGFORMAT, level=logging.INFO)


class Cmanager(BaseManager, FrontendManager):

    def __init__(self):
        BaseManager.__init__(self)
        self.stdin_path = settings.DAEMON_STDIN_PATH
        self.stdout_path = settings.DAEMON_STDOUT_PATH
        self.stderr_path = settings.DAEMON_STDERR_PATH
        self.pidfile_path = settings.DAEMON_PIDFILE_PATH
        self.pidfile_timeout = 5
        # Connect to redis
        self.redis = redis.StrictRedis(
            host=settings.REDIS_HOST,
            port=settings.REDIS_PORT,
            db=settings.REDIS_DB)

    # TODO: Rewrite to SCAN func
    def _get_redis_keys(self, pattern):
        keys = self.redis.keys(pattern)
        return keys

    def start_sites(self, site_names):
        for site_name in site_names:
            self.start_site(site_name)

    def init_sites_from_db(self):
        sites = self._get_redis_keys('*')
        self.start_sites(sites)

    def on_msg_create_site(self, channel, method_frame, header_frame, body):
        logging.info('Received msg from create_site queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        site_name = msg['site_name']
        self.create_site(
            msg['site_name'],
            msg['db_name'],
            msg['db_user'],
            msg['db_pass'],
            msg['db_prefix'],
            msg['path_to_joomla'],
            msg['path_to_sqldump'],
            msg['path_to_tmpl_conf'])

    def on_msg_remove_site(self, channel, method_frame, header_frame, body):
        logging.info('received msg from remove_site queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        site_name = msg['site_name']
        self.remove_site(site_name)

    def on_msg_start_site(self, channel, method_frame, header_frame, body):
        logging.info('received msg from start_site queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        site_name = msg['site_name']
        self.start_site(site_name, settings.PHP_BINDS)

    def on_msg_stop_site(self, channel, method_frame, header_frame, body):
        logging.info('received msg from stop_site queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        site_name = msg['site_name']
        self.stop_site(site_name, settings.PHP_BINDS)

    def on_msg_restart_site(self, channel, method_frame, header_frame, body):
        logging.info('received msg from restart_site queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        site_name = msg['site_name']
        self.restart_site(site_name, settings.PHP_BINDS)

    def on_msg_frontend(self, channel, method_frame, header_frame, body):
        logging.info('received msg from frontend queue')
        msg = json.loads(body)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        command = msg['command']
        if command == 'start':
            self.start_frontend()
        elif command == 'stop':
            self.stop_frontend()
        elif command == 'restart':
            self.restart_frontend()

    def run(self):
        # Connect RabbitMQ
        queue_connection = pika.BlockingConnection()
        queue_channel = queue_connection.channel()
        for tqueue in settings.QUEUES:
            queue_channel.queue_declare(queue=tqueue, durable=True)
        queue_channel.basic_consume(self.on_msg_create_site, 'create_site')
        queue_channel.basic_consume(self.on_msg_start_site, 'start_site')
        queue_channel.basic_consume(self.on_msg_stop_site, 'stop_site')
        queue_channel.basic_consume(self.on_msg_restart_site, 'restart_site')
        queue_channel.basic_consume(self.on_msg_remove_site, 'remove_site')
        queue_channel.basic_consume(self.on_msg_frontend, 'frontend')
        queue_channel.start_consuming()


if __name__ == '__main__':
    app = Cmanager()
    daemon_runner = runner.DaemonRunner(app)
    daemon_runner.do_action()
