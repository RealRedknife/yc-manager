#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import MySQLdb
from MySQLdb import escape_string
import logging

import settings
import helper


def _exec_sql(sql):
    logging.info('try: ' + sql)
    mysql = MySQLdb.connect(host=settings.MYSQL_HOST,
                            unix_socket=settings.MYSQL_SOCKET,
                            user=settings.MYSQL_USER,
                            db="mysql",
                            passwd=settings.MYSQL_PASS)
    cursor = mysql.cursor()
    cursor.execute(sql)
    cursor.close()


def create_mysql_db(dbname):
    dbname = escape_string(dbname)
    logging.info('create db ' + dbname)
    sql = 'CREATE DATABASE IF NOT EXISTS ' + dbname + ';'
    _exec_sql(sql)


def create_mysql_user(username, userpass):
    username = escape_string(username)
    userpass = escape_string(userpass)
    logging.info('create mysql user ' + username + ' with pass ' + userpass)
    sql = "CREATE USER '" + username + \
        "'@'localhost' IDENTIFIED BY '" + userpass + "';"
    _exec_sql(sql)


def grant_privileges(user, dbname):
    user = escape_string(user)
    dbname = escape_string(dbname)
    sql = "GRANT ALL PRIVILEGES ON " + dbname + \
        " . * TO '" + user + "'@'localhost'"
    _exec_sql(sql)


def drop_db(dbname):
    dbname = escape_string(dbname)
    logging.info('drop db ' + dbname)
    sql = 'DROP DATABASE IF EXISTS ' + dbname + ';'
    _exec_sql(sql)


def drop_user(username):
    username = escape_string(username)
    logging.info('drop user ' + username)
    sql = "DROP USER '" + username + "'@'localhost';"
    _exec_sql(sql)


def import_sql(user, userpass, dbname, db_prefix, sqlfile):
    mysql = MySQLdb.connect(host=settings.MYSQL_HOST,
                            unix_socket=settings.MYSQL_SOCKET,
                            user=user,
                            db=dbname,
                            passwd=userpass)
    cursor = mysql.cursor()
    _file = open(sqlfile, 'r')
    tmp_sql = " ".join(_file.readlines())
    sql = helper.remake_string(tmp_sql, {'db_prefix': db_prefix})
    cursor.execute(sql)
