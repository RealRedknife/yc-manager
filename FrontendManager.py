#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import logging

from BaseManager import BaseManager
import settings


class FrontendManager(BaseManager):

    def __init__(self):
        BaseManager.__init__(self)

    def start_frontend(self):
        if not self.container_exist(settings.SERVER_DOMAIN):

            ENV_OPTIONS = {
                'APP_NAME': settings.SERVER_DOMAIN,
                'APP_USER': 'yacr_user',
                'APP_GROUP': 'yacr_user',
                'APP_LISTEN_OWNER': 'www-data',
                'APP_LISTEN_GROUP': 'www-data'
            }

            self.docker.create_container(image=settings.FRONTEND_IMG,
                                         detach=True,
                                         name=settings.SERVER_DOMAIN,
                                         environment=ENV_OPTIONS,
                                         host_config={'network_mode': 'host'})
            logging.info(settings.SERVER_DOMAIN + ' created')
            self.start_site(settings.SERVER_DOMAIN, settings.FRONTEND_BIND)
        else:
            logging.info(settings.SERVER_DOMAIN + ' exist')

    def restart_frontend(self):
        self.restart_site(settings.SERVER_DOMAIN)

    def stop_frontend(self):
        self.stop_site(settings.SERVER_DOMAIN)
