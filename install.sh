#!/bin/sh

apt-get update && apt-get install -y --force-yes \
    python2.7 \
    python-pip \
    python-mysqldb \
    supervisor

cp supervisor.conf /etc/supervisor/conf.d/supervisor.conf

# install docker requirements
apt-get install -y --force-yes \
    aufs-tools cgroupfs-mount git git-man \
    libapparmor1 liberror-perl libnih-dbus1 \
    libnih1 makedev mountall plymouth \
    btrfs-tools debootstrap lxc


# install docker
wget https://get.docker.com/builds/Linux/x86_64/docker-latest -O /usr/local/bin/docker
chmod +x /usr/local/bin/docker


# install python libs
pip install docker-py
pip install redis
pip install python-daemon


# install redis
cd /tmp
wget http://download.redis.io/releases/redis-2.8.19.tar.gz
tar xzf redis-2.8.19.tar.gz
rm redis-2.8.19.tar.gz
cd redis-2.8.19
make

cp -r ./src /opt/redis
rm -rf /tmp/redis-2.8.19

ln -s /opt/redis/redis-server /usr/local/bin/redis-server
ln -s /opt/redis/redis-cli /usr/local/bin/redis-cli
