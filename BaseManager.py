#!/usr/bin/python2.7
# -*- coding: utf-8 -*-


import sys
import logging
from os import path
from shutil import rmtree, copytree

from docker.client import Client

import settings
import helper
import mysql_utils


if sys.version_info.major < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')


class BaseManager:

    def __init__(self):
        # Connect to docker
        self.docker = Client(base_url=settings.DOCKER_SOCKET)

    def container_exist(self, c_name):
        cont = self.docker.containers(filters={'name': c_name}, all=True)
        if len(cont):
            return True
        else:
            return False

    def container_running(self, c_name):
        cont = self.docker.containers(filters={'name': c_name})
        if len(cont):
            return True
        else:
            return False

    def create_site_data(self, site_name, db_name, db_user, db_pass, db_prefix, path_to_joomla, path_to_sqldump, path_to_tmpl_conf):
        host_site_folder = path.join(settings.DIRS_STRUCT['sites_data'],
                                     helper.get_site_folder(site_name))
        if not path.exists(host_site_folder):
            copytree(path_to_joomla, host_site_folder)

            db_conf = {
                'mysql_user': db_user,
                'mysql_db': db_name,
                'mysql_pass': db_pass,
                'db_prefix': db_prefix
            }

            mysql_utils.create_mysql_db(db_conf['mysql_db'])
            mysql_utils.create_mysql_user(db_conf['mysql_user'],
                                          db_conf['mysql_pass'])
            mysql_utils.grant_privileges(db_conf['mysql_user'],
                                         db_conf['mysql_db'])
            mysql_utils.import_sql(db_conf['mysql_user'],
                                   db_conf['mysql_pass'],
                                   db_conf['mysql_db'],
                                   db_conf['db_prefix'],
                                   path_to_sqldump)

            helper.rewrite_joomla_conf(path_to_tmpl_conf,
                                       db_conf)

    def remove_site(self, site_name):
        self.docker.remove_container(site_name, force=True)
        site_folder = path.join(settings.DIRS_STRUCT['sites_data'],
                                helper.get_site_folder(site_name))
        site_sockets = path.join(settings.DIRS_STRUCT['sockets_phpfpm'],
                                 site_name)
        site_logs = path.join(settings.DIRS_STRUCT['logs_phpfpm'],
                              site_name)
        site_db = helper.get_site_dbname(site_name)
        site_dbuser = helper.get_site_dbuser(site_name)
        mysql_utils.drop_db(site_db)
        mysql_utils.drop_user(site_dbuser)
        rmtree(site_sockets)
        rmtree(site_logs)
        rmtree(site_folder)

    def create_site(self, site_name, db_name, db_user, db_pass, db_prefix, path_to_joomla, path_to_sqldump, path_to_tmpl_conf):
        logging.info('try create ' + site_name)
        if not self.container_exist(site_name):
            self.create_site_data(site_name, db_name, db_user, db_pass,
                                  db_prefix, path_to_joomla, path_to_sqldump, path_to_tmpl_conf)

            ENV_OPTIONS = {
                'APP_NAME': site_name
            }

            self.docker.create_container(image=settings.PHP_IMG,
                                         detach=True,
                                         name=site_name,
                                         environment=ENV_OPTIONS)
            logging.info(site_name + ' created')
            self.start_site(site_name, settings.PHP_BINDS)
        else:
            logging.info(site_name + ' exist')

    def start_site(self, site_name, binds):
        logging.info('try start ' + site_name)
        if not self.container_running(site_name):
            regexp_dict = {
                'site_folder': helper.get_site_folder(site_name),
                'site_name': site_name
            }
            cont_binds = {}
            for index, val in binds.iteritems():
                new_index = helper.remake_string(index, regexp_dict)
                new_val = helper.remake_string(val['bind'], regexp_dict)
                cont_binds[new_index] = {'bind': new_val, 'ro': val['ro']}

            self.docker.start(site_name,
                              binds=cont_binds,
                              restart_policy=settings.RESTART_POLICY)
            logging.info(site_name + ' started')
        else:
            logging.info(site_name + ' already running')

    def stop_site(self, site_name):
        logging.info('try stop ' + site_name)
        if self.container_running(site_name):
            self.docker.stop(site_name)
            logging.info(site_name + ' stopped')
        else:
            logging.info(site_name + ' already stopped')

    def restart_site(self, site_name):
        logging.info('try restart ' + site_name)
        if self.container_running(site_name):
            self.docker.restart(site_name)
            logging.info(site_name + ' restarted')
        else:
            logging.info(site_name + ' not running')
